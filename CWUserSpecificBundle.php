<?php

namespace CW\UserSpecificBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class CWUserSpecificBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/UserSpecific/js/pimcore/startup.js'
        ];
    }
}
