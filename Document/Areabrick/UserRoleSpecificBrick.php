<?php

namespace CW\UserSpecificBundle\Document\Areabrick;

use CW\PimcoreBundle\Service\SiteConfiguration;
use Pimcore\Extension\Document\Areabrick\AbstractTemplateAreabrick;
use Pimcore\Model\Document\Tag\Area\Info;
use Pimcore\Tool\Frontend;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class UserRoleSpecificBrick extends AbstractTemplateAreabrick
{
    use ControllerTrait;

    protected $hasContainer = false;

    /** @var SiteConfiguration */
    protected $siteConfigurationService;

    /** @var AuthorizationChecker  */
    protected $authorizationChecker;

    public function __construct(SiteConfiguration $siteConfigurationService, AuthorizationChecker $authorizationChecker)
    {
        $this->siteConfigurationService = $siteConfigurationService;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'UserRoleSpecificBrick';
    }

    public function action(Info $info)
    {


        $view = $info->getView();
        // TODO: configure in config.yml
        $userroles = [
            ['ROLE_MERCHANT' , 'Merchant']
        ];
        $brickIndex = $info->getIndex();
        $user = $this->getUser();
        $siteConfiguration = $this->siteConfigurationService->getSiteConfiguration();

        $view->defaultAreaBrickConfiguration = $siteConfiguration->getDefaultAreablockOptions('default');
        $view->rolesStore = $userroles;
        $view->authorizationChecker = $this->authorizationChecker;
        $view->brickIndex = $brickIndex;
        $view->user = $user;


        //$parameters = $view->getParameters();
        //$document = $parameters->get('document');
        //$elements = $document->getElements();

        return $view;
    }

    public function getDescription()
    {
        return 'Brick for userrolespecific Elements';
    }

    public function getTemplateLocation()
    {
        $templateLocation = $this->container->getParameter('cw_user_specific.template_location');
        return $templateLocation ? $templateLocation : static::TEMPLATE_LOCATION_BUNDLE;
    }

    public function getHtmlTagOpen(Info $info)
    {
        return '';
    }

    public function getHtmlTagClose(Info $info)
    {
        return '';
    }
}