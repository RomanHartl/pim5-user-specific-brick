<?php
/** @var $view \Symfony\Component\Form\FormView */
if ($editmode): ?>
    <?= $this->select("AllowedRole", [
            "store" => $view->rolesStore,
            "width" => 400
        ]
    ); ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->areablock('defaultArea', $view->defaultAreaBrickConfiguration) ?>
        </div>
    </div>
<?php else: ?>
    <?php if ($user): ?>
        <?php if ($view->authorizationChecker->isGranted($this->select("AllowedRole")->getValue())
            && $view->user->getIsValid()
            && $view->user->getIsActive()) : ?>
            <?= $this->areablock('defaultArea') ?>
        <?php endif; ?>
    <?php endif; ?>
<?php endif; ?>
