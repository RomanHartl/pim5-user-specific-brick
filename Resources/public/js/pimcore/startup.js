pimcore.registerNS("pimcore.plugin.CWUserSpecificBundle");

pimcore.plugin.CWUserSpecificBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.CWUserSpecificBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("CWUserSpecificBundle ready!");
    }
});

var CWUserSpecificBundlePlugin = new pimcore.plugin.CWUserSpecificBundle();
